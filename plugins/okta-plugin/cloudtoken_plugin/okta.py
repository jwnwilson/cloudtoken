"""OKTA plugin for cloudtoken.

You will need to add the key 'okta_url' with the value being the URL for your OKTA IdP, for example:

okta_url: !!str 'https://{{example}}.okta.com
Author: Andy Loughran (andy@lockran.com)
"""

import argparse
import json
import re
import os
from os import system
from platform import system as platform
from urllib import parse

import requests
from bs4 import BeautifulSoup


class Plugin(object):
    """Provides an interface to okta in order to get a saml token for authenticating to AWS.

    Returns:
        data -- An object that the parent Cloudtoken process uses for authenticating to AWS.
    """
    REFRESH_COUNT = 0

    def __init__(self, config):
        """
        Sets up a new CloudToken OKTA Plugin instance,
        using variables defined in the self properties below.

        Arguments:
            config -- parsed config.yml file (provided by cloudtoken)
        """
        self._config = config
        self._name = 'okta'
        self._description = 'Authenticate against OKTA.'
        self._authurl = "api/v1/authn"
        self._headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        self._content_options = {
            'multiOptionalFactorEnroll': 'true',
            'warnBeforePasswordExpired': 'true'
        }
        try:
            self._url = config['okta_url']
            re.search(r'(https?://[\w\-.]+)/', self._url).group(1)
        except KeyError:
            print("Configuration key 'okta_url' not found. Exiting.")
            exit(1)
        except AttributeError:
            print(
                "Configuration key 'okta_url' value does not seem to be a http(s) URL. Exiting.")
            exit(1)

    def __str__(self):
        """Return the self string"""
        return __file__

    @staticmethod
    def unset(args):
        """Framework function"""
        pass

    @staticmethod
    def arguments(defaults):
        """parses the arguments passed to the plugin"""
        parser = argparse.ArgumentParser()
        return parser

    def notify_okta_requires_input(self, title, text):
        """Notify the user that we require input.
        """
        # OS X
        if platform == "darwin":
            resp = os.system(f"""
              osascript -e 'display dialog "{text}" with title "{title}"'
              """)
        else:
            print(title, text)
            resp = 0

        # If user cancels then raise close app exception
        if resp != 0:
            exit(1)

    def execute(self, data, args, flags):
        """The main execution of the program.

        Initial call to authenticate against OKTA.
        Second call with MFA to get Token.
        """
        url = self._url

        if self.REFRESH_COUNT > 0:
            self.notify_okta_requires_input("Cloud Token Input Required", "Your okta access is about to expire please re-enter your MFA code into cloudtoken.")
        self.REFRESH_COUNT += 1

        # GET MFA Code
        selection = input("Please enter your MFA code: ")

        session = requests.session()
        content = {'username': args.username, 'password': args.password, 'options': self._content_options}

        full_url = "https://{}/{}".format(
            parse.urlparse(url).netloc,
            self._authurl
        )

        response_data = session.post(full_url, headers=self._headers,
                                     data=json.dumps(content))
        saml_response = json.loads(response_data.text)
        try:
            token = saml_response['stateToken']
            auth_id = saml_response['_embedded']['factors'][0]['id']
        except KeyError:
            print(f"There has been an authentication error: {saml_response}")
            exit(1)

        verify_url = "{}/factors/{}/verify".format(full_url, auth_id)
        verification_content = {'stateToken': token}
        verification_content['passCode'] = selection

        # Post MFA code to get Token.
        verification_response = session.post(
            verify_url,
            headers=self._headers,
            data=json.dumps(verification_content)
        )
        session_token = json.loads(verification_response.text)['sessionToken']
        saml_url = "{}?sessionToken={}".format(url, session_token)
        saml_response = session.get(saml_url)

        soup = BeautifulSoup(saml_response.text, "html.parser")
        saml = soup.find("input", attrs={"name": "SAMLResponse"})
        response_data = saml['value']
        data.append({'plugin': self._name, 'data': response_data})
        return data
