pyyaml==3.12
Flask==0.12.2
schedule==0.5.0
keyring==12.2.0
requests>=2.9.1
